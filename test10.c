#include<stdio.h>
int main()
{
    int *a, *b, x, y;
    a = &x;
    b = &y;
    printf("Enter the value of a and b: ");
    scanf("%d %d", a, b);
    printf("\nThe sum is: %d", *a+*b);
    printf("\nThe difference is: %d", *a-*b);
    printf("\nThe product is: %d", (*a)*(*b));
    printf("\nThe quotient is: %d", (*a)/(*b));
    printf("\nThe remainder is: %d", *a%*b);
    return 0;
}