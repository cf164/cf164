#include<stdio.h>
int main()
{
    int a[50], i,n, val, flag = 0, pos= -1;
    printf("Enter the number of elements: ");
    scanf("%d", &n);
    for(i = 0; i < n; i++)
        scanf("%d", &a[i]);
    printf("\n Enter the value to be searched: ");
    scanf("%d",&val);
    for(i = 0; i < n; i++)
        if(a[i] == val)
        {
           flag = 1;
           pos = i+1;
           break;
        }
    if(flag == 1)
        printf("number found in position %d", pos);
    else
        printf("number not found");
    return 0;
}
