#include<stdio.h>
float cal_average(float arr[50], int n)
{    
   float sum = 0, av;
   for(int j = 1; j <= n; j++)
    sum += arr[j];
   av = sum/n;
   return av;
}
int main()
{
    int n; float arr[50], av;
    printf("Enter the total no of values: ");
    scanf("%d", &n);
    for(int i = 1; i <= n; i++)
    {
        printf("Enter value %d: ", i);
        scanf("%f", &arr[i]);
       
    }
    av = cal_average(arr, n);
    printf("The average is : %f", av);
    return 0;
}
