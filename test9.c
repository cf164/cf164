#include<stdio.h>
int main()
{
    struct stu_det{int rno, fees, res; char sec, nam[20], dep[20]; };
    struct stu_det s1, s2;
    printf("Enter students roll numbers: ");
    scanf("%d%d", &s1.rno, &s2.rno);
    printf("Enter students fees: ");
    scanf("%d%d", &s1.fees, &s2.fees);
    printf("Enter students results: ");
    scanf("%d%d", &s1.res, &s2.res);
    printf("Enter students sections: ");
    scanf(" %c %c", &s1.sec, &s2.sec);
    printf("Enter students names: ");
    scanf("%s%s", &s1.nam, &s2.nam);
    printf("Enter students departments: ");
    scanf("%s%s", &s1.dep, &s2.dep);
    if(s1.res > s2.res)
    {
        printf("\nStudent 1 got the higher score");
        printf("\nStudent roll no: %d", s1.rno);
        printf("\nStudent fees: %d", s1.fees);
        printf("\nStudent results: %d", s1.res);
        printf("\nStudent section: %c", s1.sec);
        printf("\nStudent name: %s", s1.nam);
        printf("\nStudent department: %s", s1.dep);
    }
    else if(s2.res > s1.res)
    {
        printf("\nStudent 2 got the higher score");
        printf("\nStudent roll no: %d", s2.rno);
        printf("\nStudent fees: %d", s2.fees);
        printf("\nStudent results: %d", s2.res);
        printf("\nStudent section: %c", s2.sec);
        printf("\nStudent name: %s", s2.nam);
        printf("\nStudent department: %s", s2.dep);
    }
    else
        printf("\nBoth received equal score");
    return 0;
}
