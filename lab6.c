#include<stdio.h>
int main()
{
    int marks[5][3],i,j, max;
    for(i = 0; i < 5; i++)
        for(j = 0; j < 3; j++)
        {
            printf("Enter the mark %d of student %d: ",j+1, i+1);
            scanf("%d", &marks[i][j]);
        }
    for(i = 0; i < 5; i++)
    {
        max = marks[i][0];
        for(j = 0; j < 3; j++)
            if(marks[i][j] > max)
                max = marks[i][j];
        printf("\nThe maximum marks of student %d is : %d",i+1,max);
    }
    return 0;
}