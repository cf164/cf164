#include<stdio.h>
int main()
{
    char str[20]; //array to store a word
    int i = 0,n = 0, temp = 1;
    printf("Enter string: ");
    scanf("%s", str); //taking input in variable str
    while(str[n] != '\0') //while loop to count the number os letters in the word and store it in n
        n++;
    for(i = 0; i < n/2; i++) // for loop to check if a letter from the beginning is the same as the letter from the ending
       if(str[i] != str[n-1-i])
        {
            temp = 0; //flag variable to check if it isn't a palindrome
            break;
        }
    if(temp == 0)
        printf("Not Palindrome");
    else
        printf("Palindrome");
    return 0;
}