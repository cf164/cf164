#include<stdio.h>
int main()
{
    FILE *f;
    char ch;
    f = fopen("INPUT.txt", "w");
    printf("Enter the text: \n");
    while((ch = getchar())!= EOF)
    {
        putc(ch,f);
    }
    fclose(f);
    f = fopen("INPUT.txt", "r");
    printf("The contents are: \n");
    while((ch = getc(f)) != EOF)
    {
        printf("%c", ch);
    }
    fclose(f);
}