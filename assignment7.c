#include<stdio.h>
int main()
{
    int a[50][50],i,j, sum1 = 0, sum2 = 0;
    for(i = 0; i < 4; i++)
        for(j = 0; j < 3; j++)
        {
            printf("Enter sales of product %d of salesman %d: ",j+1, i+1);
            scanf("%d", &a[i][j]);
        }
    for(i = 0; i < 4; i++)
    {
        sum1 = 0;
        for(j = 0; j < 3 ; j++)
        {
            sum1 += a[i][j];
        }
        printf("\nThe total sales of salesman %d is : %d ",i+1,sum1);
    }
    for(i = 0; i < 3; i++)
    {
        sum2 = 0;
        for(j = 0; j < 4 ; j++)
        {
            sum2 += a[j][i];
        }
        printf("\nThe total sales of product %d is : %d ",i+1,sum2);
    }
    return 0;
}