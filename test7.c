#include<stdio.h>
int main()
{
    int a[50][50], t[50][50], i,j,m,n;
    printf("Enter the number of rows: ");
    scanf("%d", &m);
    printf("Enter the number of columns: ");
    scanf("%d", &n);
    for(i = 0; i < m; i++)
        for(j = 0; j < n; j++)
        {
            printf("Enter value %d of row %d: ",j+1, i+1);
            scanf("%d", &a[i][j]);
        }
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < m ; j++)
         {   
            t[i][j] = a[j][i];
            printf("%d ",t[i][j]);
         }
        printf("\n");
    }
    return 0;
}