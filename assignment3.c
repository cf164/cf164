#include<stdio.h>
int main()
{
 float m1,m2,m3,m4,m5,av;
 printf("Enter the marks for all 5 courses:");
 scanf("%f%f%f%f%f",&m1,&m2,&m3,&m4,&m5);
 av = (m1+m2+m3+m4+m5)/5;
 if(m1<40 || m2<40 || m3<40 || m4<40 || m5<40)
 printf("F grade");
 else
 {
   if(av >= 90 && av <= 100)
     printf("S grade");
   else if(av >= 80 && av <= 89)
     printf("A grade");
   else if(av >= 70 && av <= 79)
     printf("B grade");
   else if(av >= 60 && av <= 69)
     printf("C grade");
   else if(av >= 50 && av <= 59)
     printf("D grade");
   else if(av >= 40 && av <= 49)
     printf("E grade");
   else
     printf("Invalid input");
 }
 return 0;
}