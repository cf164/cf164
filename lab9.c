#include<stdio.h>
void swap(int *a, int *b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}
int main()
{
    int x, y;
    printf("Enter the value of a and b: ");
    scanf("%d %d", &x,&y);
    printf("\nBefore swapping: a : %d b: %d", x,y);
    swap(&x,&y);
    printf("\nAfter swapping: a : %d b: %d", x,y);
    return 0;
}
