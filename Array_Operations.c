#include<stdio.h>
void insertion()
{
    int a[50],n,pos,num,i;
    printf("\nEnter the number of elements: ");
    scanf("%d",&n);
    for(i = 0; i < n; i++)
    {
        printf("Enter element %d : ",i+1);
        scanf("%d",&a[i]);
    }
    printf("\nEnter the number to be inserted: ");
    scanf("%d",&num);
    printf("\nEnter the position: ");
    scanf("%d",&pos);
    for(i = n; i > pos-1; i--)
        a[i] = a[i-1];
    a[i] = num;
    printf("\nThe final array is : \n");
    for(int i = 0; i <= n; i++)
       printf("%d ",a[i]);
}
void deletion()
{
    int a[50],n,num,i;
    printf("\nEnter the number of elements: ");
    scanf("%d",&n);
    for(i = 0; i < n; i++)
    {
        printf("Enter element %d : ",i+1);
        scanf("%d",&a[i]);
    }
    printf("\nEnter the number to be deleted: ");
    scanf("%d",&num);
    for(i = 0; i < n; i++)
        if(a[i] == num)
        {
            for(int j = i; j < n; j++)
                a[j] = a[j+1];
            n--;
        }
    printf("\nThe final array is : \n");
    for(int i = 0; i < n; i++)
       printf("%d ",a[i]);
}
void linearsearch()
{
    int a[50],n,pos,num,i;
    printf("\nEnter the number of elements: ");
    scanf("%d",&n);
    for(i = 0; i < n; i++)
    {
        printf("Enter element %d : ",i+1);
        scanf("%d",&a[i]);
    }
    printf("\nEnter the number to be searched: ");
    scanf("%d",&num);
    for(i = 0; i < n; i++)
    {
        if(a[i] == num)
            pos = i+1;
    }
    printf("\nNumber found in position : %d",pos);
}
void binarysearch()
{
    int a[50],n,num,i, min, max,mid, pos;
    printf("\nEnter the number of elements: ");
    scanf("%d",&n);
    for(i = 0; i < n; i++)
    {
        printf("Enter element %d : ",i+1);
        scanf("%d",&a[i]);
    }
    printf("\nEnter the number to be searched: ");
    scanf("%d",&num);
    min = 0; max = n-1;
    while(min <= max)
    {
      mid = (min + max)/2;
      if(a[mid] == num)
      {
        pos = mid+1;
        break;
      }
      else if(a[mid] > num)
            max = mid-1;
      else
            min = mid+1;
    }
    printf("\nNumber found in position : %d",pos);
}
void transpose()
{
    int a[50][50],i,j,m,n;
    printf("Enter the number of rows: ");
    scanf("%d", &m);
    printf("Enter the number of columns: ");
    scanf("%d", &n);
    for(i = 0; i < m; i++)
        for(j = 0; j < n; j++)
        {
            printf("Enter value %d of row %d: ",j+1, i+1);
            scanf("%d", &a[i][j]);
        }
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < m ; j++)
            printf("%d ",a[j][i]);
        printf("\n");
    }
}
void addition()
{
    int a[50][50], b[50][50], c[50][50],i,j, m, n;
    printf("Enter the number of rows: ");
    scanf("%d", &m);
    printf("Enter the number of columns: ");
    scanf("%d", &n);
    printf("\nMatrix 1: \n");
    for(i = 0; i < m; i++)
        for(j = 0; j < n; j++)
        {
          printf("Enter value %d of row %d: ",j+1, i+1);
          scanf("%d", &a[i][j]);
        }
    printf("\nMatrix 2: \n");
    for(i = 0; i < m; i++)
        for(j = 0; j < n; j++)
        {
          printf("Enter value %d of row %d: ",j+1, i+1);
          scanf("%d", &b[i][j]);
          c[i][j] = a[i][j] + b[i][j];
        }
    printf("\nThe matrix sum is: \n");
    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n; j++)
            printf("%d ",c[i][j]);
        printf("\n");
    }
}
void subtraction()
{
  int a[50][50], b[50][50], c[50][50],i,j, m, n;
    printf("Enter the number of rows: ");
    scanf("%d", &m);
    printf("Enter the number of columns: ");
    scanf("%d", &n);
    printf("\nMatrix 1: \n");
    for(i = 0; i < m; i++)
        for(j = 0; j < n; j++)
        {
          printf("Enter value %d of row %d: ",j+1, i+1);
          scanf("%d", &a[i][j]);
        }
    printf("\nMatrix 2: \n");
    for(i = 0; i < m; i++)
        for(j = 0; j < n; j++)
        {
          printf("Enter value %d of row %d: ",j+1, i+1);
          scanf("%d", &b[i][j]);
          c[i][j] = a[i][j] - b[i][j];
        }
    printf("\nThe matrix sum is: \n");
    for(i = 0; i < m; i++)
    {
        for(j = 0; j < n; j++)
            printf("%d ",c[i][j]);
        printf("\n");
    }
}
int main()
{
    int m;
    do
    {
        printf("\nMENU");
        printf("\n1.Insert an element");
        printf("\n2.Delete an element");
        printf("\n3.Linear Search");
        printf("\n4.Binary Search");
        printf("\n5.Transpose of Matrix");
        printf("\n6.Addition");
        printf("\n7.Subtraction");
        printf("\n8.Exit");
        printf("\nEnter a value: ");
        scanf("%d",&m);
        switch(m)
        {
            case 1: insertion();
                    break;
            case 2: deletion();
                    break;
            case 3: linearsearch();
                    break;
            case 4: binarysearch();
                    break;
            case 5: transpose();
                    break;
            case 6: addition();
                    break;
            case 7: subtraction();
                    break;
        }
    }while(m!=8);
    return 0;
}