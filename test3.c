#include<stdio.h>
#include<math.h>
int main()
{
float a,b,c,d,r1,r2;
printf("Enter the a,b and c values of equation: ");
scanf("%f%f%f",&a,&b,&c);
d = (b*b - 4*a*c);
if(a == 0)
  printf("Not a quadratic equation");
else
 {
    if (d > 0)
    {
     r1 = (-b + sqrt(d))/(2*a);
     r2 = (-b - sqrt(d))/(2*a);
     printf("The roots are real and distinct\n");
     printf("The roots are %f and %f", r1,r2);
    }
    else if (d == 0)
    {
     r1 = -b/(2*a);
     printf("The roots are real and equal\n");
     printf("The root is %f",r1);
    }
    else 
    {
     r1 = (-b + sqrt(fabs(d)))/(2*a);
     r2 = (-b - sqrt(fabs(d)))/(2*a);
     printf("The roots are complex and distinct\n");
     printf("The roots are %f and %f", r1,r2);
    }
 }
return 0;
}